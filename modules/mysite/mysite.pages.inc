<?php

/**
 * @file
 * Display custom pages for this website.
 *
 * @see mysite.module
 */

/**
 * Page callback for the courses list.
 */
function mysite_courses_page() {
  drupal_set_title(t('CISCO Courses from the Current College Catalog'));

  $filter_opts = array('subject=CNT');

  $courses = hfccwsclient_get_courses($filter_opts);

  if (is_array($courses)) {
    $rows = array();
    foreach ($courses as $id => $course) {
      $rows[$id] = $course + array('#theme' => 'hfccwsclient_course');
    }
    return $rows;
  }
  else {
    return t('An error occurred while retrieving the course listing. Please try again later.');
  }
}
